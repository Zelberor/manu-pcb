
# Plugin for exporting a pcb from within KiCad

## Installation
- Make sure the *manu-pcb* executable is installed and added to the *PATH*
- Open Pcb Editor and open the Python console. Execute
`import pcbnew; print(pcbnew.PLUGIN_DIRECTORIES_SEARCH)`
- Copy the [manu-pcb](./manu-pcb) folder in one of the folders listed.

## Usage
- Export the pcb to manu-pcb with **Tools->External Plugins->Manu-Pcb**
- The exported files end up inside `<ProjectDir>/export/manu-pcb`
- Optional: Modify the dxf files if necessary
- Run the `generate_gcode.sh` script in this folder to create the gcode.
