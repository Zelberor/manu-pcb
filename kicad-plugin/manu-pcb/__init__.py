import os
import stat

import pcbnew
import glob

import sys
import importlib

import pcbnew


def reimport():
    importlib.reload(sys.modules[__name__])


def export():
    board: pcbnew.BOARD = pcbnew.GetBoard()
    project_path = board.GetFileName()
    project_name = os.path.splitext(os.path.basename(project_path))[0]
    print(f'Processing project {project_name}...')
    project_dir = os.path.dirname(project_path)
    dxf_dir = os.path.join(project_dir, 'export', 'manu-pcb')
    os.makedirs(dxf_dir, exist_ok=True)

    pctl = pcbnew.PLOT_CONTROLLER(board)
    popt: pcbnew.PCB_PLOT_PARAMS = pctl.GetPlotOptions()
    popt.SetOutputDirectory(dxf_dir)

    # Set some important plot options (see pcb_plot_params.h):
    popt.SetPlotFrameRef(False)  # do not change it
    popt.SetSketchPadLineWidth(pcbnew.FromMM(0.1))

    popt.SetPlotReference(True)
    popt.SetPlotValue(True)

    popt.SetAutoScale(False)
    popt.SetScale(1)
    popt.SetMirror(False)
    popt.SetPlotInvisibleText(False)
    popt.SetSubtractMaskFromSilk(False)
    popt.SetUseAuxOrigin(True)

    # Disable plot pad holes
    popt.SetDrillMarksType(pcbnew.DRILL_MARKS_NO_DRILL_SHAPE)
    popt.SetSkipPlotNPTH_Pads(True)
    popt.SetDXFPlotPolygonMode(True)
    popt.SetDXFPlotUnits(pcbnew.DXF_UNITS_MILLIMETERS)

    plot_plan = [
        ('F_Cu', pcbnew.F_Cu, 'Front copper'),
        ('B_Cu', pcbnew.B_Cu, 'Back copper'),
        ('F_Mask', pcbnew.F_Mask, 'Front mask'),
        ('B_Mask', pcbnew.B_Mask, 'Back mask'),
        ('Edge_Cuts', pcbnew.Edge_Cuts, 'Edges'),
    ]

    gerber_file_paths = {}

    for layer_info in plot_plan:
        if layer_info[0] == 'Edge_Cuts':
            popt.SetDXFPlotPolygonMode(False)
        else:
            popt.SetDXFPlotPolygonMode(True)

        pctl.SetLayer(layer_info[1])
        pctl.OpenPlotfile(
            layer_info[0], pcbnew.PLOT_FORMAT_DXF, layer_info[2])
        filepath = pctl.GetPlotFileName()
        print(f'Plotting {layer_info[2]}...')
        if pctl.PlotLayer() is False:
            raise RuntimeError(f'Failed to plot {layer_info[2]}')
        gerber_file_paths[layer_info[0]] = filepath

    pctl.ClosePlot()

    drlwriter = pcbnew.EXCELLON_WRITER(board)
    drlwriter.SetMapFileFormat(pcbnew.PLOT_FORMAT_GERBER)
    mirror = False
    minimal_header = False
    offset = pcbnew.VECTOR2I(0, 0)
    merge_npth = True
    drlwriter.SetOptions(mirror, minimal_header, offset, merge_npth)
    drlwriter.SetFormat(True)
    drlwriter.SetRouteModeForOvalHoles(False)
    print(f'Plotting Drill file...')
    drlwriter.CreateDrillandMapFilesSet(dxf_dir, True, False)
    drl_files = glob.glob(os.path.join(dxf_dir, f'*.{drlwriter.GetDrillFileExt()}'))
    if len(drl_files) != 1:
        raise RuntimeError(f'Expected exactly one drill file to be generated')
    gerber_file_paths['Drill'] = drl_files[0]

    # Generate etcher script
    script_path = os.path.join(dxf_dir, 'generate_gcode.sh')
    with open(script_path, 'w') as file:
        file.write(f'''#!/bin/bash
XOFFSET=0
YOFFSET=0
echo Generating F_Cu laser gcode...
manu-pcb -e {gerber_file_paths['Edge_Cuts']} -x=$XOFFSET -y=$YOFFSET -o {project_name}-f_cu.nc -f snapmaker generate-laser -i {gerber_file_paths['F_Cu']} -d 0.25 -s 0.1 -n 1 -w 2000 --feedrate-trace 250 --dwell-before-on=100
echo Generating B_Cu laser gcode...
manu-pcb -e {gerber_file_paths['Edge_Cuts']} -x=$XOFFSET -y=$YOFFSET --mirror-x -o {project_name}-b_cu.nc -f snapmaker generate-laser -i {gerber_file_paths['B_Cu']} -d 0.25 -s 0.1 -n 1 -w 2000 --feedrate-trace 250 --dwell-before-on=100
echo Generating mill cutout gcode...
manu-pcb -e {gerber_file_paths['Edge_Cuts']} -x=$XOFFSET -y=$YOFFSET -o {project_name}-mill-cutout.cnc -f snapmaker generate-mill -d 3.175 -z=-4 -s=0.5 --tabs-every 50 --tab-offset 20 --rpm 12000
echo Generating drill gcode...
manu-pcb -e {gerber_file_paths['Edge_Cuts']} -x=$XOFFSET -y=$YOFFSET -o {project_name}-drill.cnc -f snapmaker generate-drill -i {gerber_file_paths['Drill']} --z-depth=-4 --rpm 6000
echo Generating drill-mill gcode...
manu-pcb -e {gerber_file_paths['Edge_Cuts']} -x=$XOFFSET -y=$YOFFSET -o {project_name}-drill-mill.cnc -f snapmaker generate-drill-mill -i {gerber_file_paths['Drill']} --min-hole-diameter=1.5 -d=1.5 -s=0.5 --z-depth=-4 --rpm 12000
echo Generating alignment hole...
manu-pcb -e {gerber_file_paths['Edge_Cuts']} -o {project_name}-alignment-hole.cnc -f snapmaker generate-alignment-hole --z-depth=-4 --rpm 6000
echo Done.
''')

    st = os.stat(script_path)
    os.chmod(script_path, st.st_mode | stat.S_IEXEC)


class ManuPcbPlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Manu-Pcb"
        self.category = "Export"
        self.description = "Converts pcbs into gcode for laser engraving"
        self.show_toolbar_button = False # Optional, defaults to False
        # self.icon_file_name = os.path.join(os.path.dirname(__file__), 'simple_plugin.png') # Optional, defaults to ""

    def Run(self):
        export()

ManuPcbPlugin().register() # Instantiate and register to Pcbnew
