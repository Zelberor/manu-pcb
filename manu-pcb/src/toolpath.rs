// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::VecDeque;

use nalgebra::Transform2;

use crate::geometry::{Line2D, Point, FT};

pub struct ToolLine {
	line: Line2D,
	/// Tool radius
	radius: FT,
}

impl ToolLine {
	pub fn new(line: Line2D, radius: FT) -> Self {
		ToolLine { line, radius }
	}

	pub fn get_line(&self) -> &Line2D {
		&self.line
	}

	pub fn set_line(&mut self, line: Line2D) {
		self.line = line
	}
}

pub struct ToolPath {
	points: Vec<Point>,
	/// Tool radius
	radius: FT,
}

impl ToolPath {
	pub fn new(points: &[Point], radius: FT) -> Self {
		Self { points: points.to_owned(), radius }
	}

	pub fn get_point_iter(&self) -> impl Iterator<Item = &Point> + DoubleEndedIterator {
		self.points.iter()
	}

	pub fn get_point(&self, index: usize) -> Option<&Point> {
		self.points.get(index)
	}

	pub fn mirror_x(&mut self) {
		for p in &mut self.points {
			p.x = -p.x;
		}
	}

	pub fn mirror_y(&mut self) {
		for p in &mut self.points {
			p.y = -p.y;
		}
	}

	pub fn transform(&mut self, transform: Transform2<FT>) {
		for p in &mut self.points {
			*p = transform * *p;
		}
	}

	pub fn length(&self) -> FT {
		let mut length = 0.0;

		let mut points_iter = self.get_point_iter();
		let last_point = points_iter.next();
		if let Some(mut last_point) = last_point.cloned() {
			for p in points_iter {
				let line = Line2D::new(last_point, *p);
				length += line.length();
				last_point = *p;
			}
		}

		length
	}

	pub fn split_every(self, interval: FT) -> Vec<ToolPath> {
		let mut split_tool_paths = Vec::new();

		let mut deque: VecDeque<_> = self.points.into();

		while deque.len() > 0 {
			let tool_points = split_point_list(&mut deque, interval, false);
			split_tool_paths.push(ToolPath::new(&tool_points, self.radius));
		}

		split_tool_paths
	}

	pub fn shorten_ends(mut self, distance: FT) -> Option<ToolPath> {
		let mut deque: VecDeque<_> = self.points.into();
		split_point_list(&mut deque, distance, false);
		split_point_list(&mut deque, distance, true);
		self.points = deque.into();

		if self.points.len() >= 2 {
			Some(self)
		} else {
			None
		}
	}

	pub fn circular_move_start(mut self, distance: FT) -> Self {
		let mut deque: VecDeque<_> = self.points.into();

		let mut before_points = split_point_list(&mut deque, distance, false);
		let mut vec: Vec<_> = deque.into();
		vec.pop();
		vec.append(&mut before_points);
		self.points = vec;
		self
	}
}

/// Modifies *points* to contain the elements after the split; returns the elements before the split
fn split_point_list(points: &mut VecDeque<Point>, distance: FT, start_from_last: bool) -> Vec<Point> {
	let mut before_points = Vec::new();

	let last_point = if start_from_last { points.pop_back() } else { points.pop_front() };
	if let Some(last_point) = last_point {
		let mut remaining_distance = distance;
		before_points.push(last_point);

		while let Some(p) = if start_from_last { points.pop_back() } else { points.pop_front() } {
			let line = Line2D::new(*before_points.last().unwrap(), p);
			let length = line.length();

			if remaining_distance >= length {
				remaining_distance -= length;
				before_points.push(line.p2);
			} else {
				let first_point = line.p1 + line.get_direction().into_inner() * remaining_distance;
				if remaining_distance != 0.0 {
					before_points.push(first_point);
				}
				if start_from_last {
					points.push_back(line.p2);
					points.push_back(first_point);
				} else {
					points.push_front(line.p2);
					points.push_front(first_point);
				}
				break;
			}
		}
	}
	before_points
}

impl From<&ToolLine> for ToolPath {
	fn from(value: &ToolLine) -> Self {
		ToolPath {
			points: vec![value.line.p1, value.line.p2],
			radius: value.radius,
		}
	}
}
