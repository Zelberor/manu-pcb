// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use itertools::concat;

use crate::{
	geometry::{CmpRefDistance, IntersectsWith, Line2D, Point, FT},
	outline::{self, Outline, OutlineLineIntersection},
};

pub struct LineTracer {
	tool_active: bool,
	intersections: Vec<OutlineLineIntersection>,
	current_intersection_index: usize,
	entered_coincident_section: bool,
}

impl LineTracer {
	/// offset: parallel offset to the line for the intersections
	pub fn new<'a, OI: Iterator<Item = &'a Outline>>(line: Line2D, offset: FT, outlines: OI, tool_active: bool) -> Self {
		// Calculate the intersections for the offset line
		let offset_line = line.parallel(offset);
		let mut intersections = get_sorted_intersections(&offset_line, outlines);
		// Move intersection points back to non offset line
		for i in intersections.iter_mut() {
			i.t = match i.t {
				outline::OutlineLineIntersectionType::Point(p) => outline::OutlineLineIntersectionType::Point(offset_line.move_point_rangle(p, -offset)),
				outline::OutlineLineIntersectionType::Coincident(p1, p2) => outline::OutlineLineIntersectionType::Coincident(offset_line.move_point_rangle(p1, -offset), offset_line.move_point_rangle(p2, -offset)),
			}
		}
		LineTracer {
			tool_active,
			intersections,
			current_intersection_index: 0,
			entered_coincident_section: false,
		}
	}

	pub fn get_laser_enabled(&self) -> bool {
		if self.entered_coincident_section {
			true
		} else {
			self.tool_active
		}
	}

	fn toggle_laser_enabled(&mut self) {
		self.tool_active = !self.tool_active
	}

	pub fn get_current_intersection_point(&self) -> Option<Point> {
		if self.current_intersection_index < self.intersections.len() {
			let i = &self.intersections[self.current_intersection_index];
			Some(match i.t {
				outline::OutlineLineIntersectionType::Point(p) => p,
				outline::OutlineLineIntersectionType::Coincident(p1, p2) => {
					if self.entered_coincident_section {
						p2
					} else {
						p1
					}
				}
			})
		} else {
			None
		}
	}

	pub fn advance_intersection(&mut self) -> bool {
		if self.current_intersection_index < self.intersections.len() {
			let i = &self.intersections[self.current_intersection_index];
			match i.t {
				outline::OutlineLineIntersectionType::Point(_) => {
					self.current_intersection_index += 1;
					if i.overstep {
						self.toggle_laser_enabled();
					}
				}
				outline::OutlineLineIntersectionType::Coincident(_, _) => {
					if self.entered_coincident_section {
						self.current_intersection_index += 1;
						if i.overstep {
							self.toggle_laser_enabled();
						}
						self.entered_coincident_section = false;
					} else {
						self.entered_coincident_section = true;
					}
				}
			}

			true
		} else {
			false
		}
	}

	pub fn get_final_enabled(mut self) -> (Option<Point>, bool) {
		while self.advance_intersection() {}
		(self.get_current_intersection_point(), self.get_laser_enabled())
	}
}

fn get_sorted_intersections<'a, I: Iterator<Item = &'a Outline>>(line: &Line2D, outlines: I) -> Vec<OutlineLineIntersection> {
	let mut intersections: Vec<_> = concat(outlines.map(|e| e.intersects_with(&line)));
	intersections.sort_by(|a, b| {
		let a_ip = a.get_near_point(&line.p1);
		let b_ip = b.get_near_point(&line.p1);
		a_ip.cmp_ref_distance(&b_ip, &line.p1)
	});
	intersections
}
