// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{
	f64::{INFINITY, NEG_INFINITY},
	time::Duration,
};
pub mod flavour;

use nalgebra::{distance, Translation2};

use crate::{
	drill::Hole,
	geometry::{Line2D, Point, Point3, FT},
	print::print_warning,
	toolpath::ToolPath,
};

#[derive(Debug, Clone)]
pub enum GcodeCommand {
	ToolOn { power: ToolPower },
	ToolOff,
	Dwell { time_ms: u64 },
	Move { t: MoveType, x: Option<FT>, y: Option<FT>, z: Option<FT>, feedrate: u32 },
	MoveArc { ccw: bool, center_offset: Point, end_pos: Option<Point>, feedrate: u32 },
}

#[derive(Debug, Clone)]
pub enum ToolPower {
	PowerPercentage(f32),
	SpindleRpm(u32),
}

pub enum ToolType {
	Laser,
	Cnc,
}

impl GcodeCommand {
	pub fn get_duration(&self, last_position: Point3) -> Duration {
		match self {
			GcodeCommand::ToolOn { power: _ } => Duration::from_millis(25),
			GcodeCommand::ToolOff => Duration::from_millis(25),
			GcodeCommand::Dwell { time_ms } => Duration::from_millis(*time_ms),
			GcodeCommand::Move { t: _, x, y, z, feedrate } => {
				let mm_s = *feedrate as f64 / 60.0;

				let new_position = Point3::new(x.unwrap_or(last_position.x), y.unwrap_or(last_position.y), z.unwrap_or(last_position.z));

				let distance = distance(&last_position, &new_position);

				Duration::from_secs_f64(distance / mm_s)
			}
			GcodeCommand::MoveArc { ccw: _, center_offset, end_pos: _, feedrate } => {
				// TODO: proper calculation
				let mm_s = *feedrate as f64 / 60.0;

				let distance = center_offset.coords.magnitude();
				Duration::from_secs_f64(distance / mm_s)
			}
		}
	}
}

#[derive(Debug, Clone)]
pub enum MoveType {
	/// G0
	Rapid,
	/// G1
	Tool,
}

pub fn generate_laser_commands(feedrate_move: u32, feedrate_work: u32, laser_power: f32, tool_paths: &[ToolPath], reversed: bool, overshoot_compensation: Option<FT>, dwell_before_on: Option<u64>) -> Vec<GcodeCommand> {
	let mut commands = Vec::new();
	let paths = if reversed { itertools::Either::Left(tool_paths.iter().rev()) } else { itertools::Either::Right(tool_paths.iter()) };
	for tool_path in paths {
		let points = if reversed {
			itertools::Either::Left(tool_path.get_point_iter().rev())
		} else {
			itertools::Either::Right(tool_path.get_point_iter())
		};
		let mut points = points.peekable();
		if let Some(start_point) = points.next() {
			commands.push(GcodeCommand::Move {
				t: MoveType::Rapid,
				x: Some(start_point.x),
				y: Some(start_point.y),
				z: None,
				feedrate: feedrate_move,
			});
			let mut turned_on = false;
			let mut last_point = *start_point;

			while let Some(point) = points.next() {
				if !turned_on {
					if let Some(ms) = dwell_before_on {
						commands.push(GcodeCommand::Dwell { time_ms: ms })
					}
					commands.push(GcodeCommand::ToolOn {
						power: ToolPower::PowerPercentage(laser_power),
					});
					turned_on = true;
				}

				let mut point = *point;
				if let Some(o_cmp) = overshoot_compensation {
					let is_last = points.peek().is_none();
					if is_last {
						let line = Line2D::new(point, last_point);
						point = point + line.get_direction().into_inner() * o_cmp;
					}
				}

				commands.push(GcodeCommand::Move {
					t: MoveType::Tool,
					x: Some(point.x),
					y: Some(point.y),
					z: None,
					feedrate: feedrate_work,
				});
				last_point = point;
			}
			if turned_on {
				commands.push(GcodeCommand::ToolOff)
			}
		}
	}

	commands
}

pub fn generate_mill_commands(feedrate_move: u32, feedrate_move_z: u32, feedrate_work: u32, feedrate_plunge: u32, z_depth: FT, z_step: FT, move_height: FT, safe_height: FT, spindle_rpm: u32, tool_paths: &[ToolPath]) -> Vec<GcodeCommand> {
	assert!(z_depth < 0.0);
	assert!(z_step > 0.0);

	let mut current_position = None;
	let z_steps = (z_depth / z_step).abs().ceil() as usize;

	let mut commands = Vec::new();
	commands.push(GcodeCommand::Move {
		t: MoveType::Rapid,
		x: None,
		y: None,
		z: Some(move_height),
		feedrate: feedrate_move_z,
	});
	let mut current_z_position = move_height;
	commands.push(GcodeCommand::ToolOn { power: ToolPower::SpindleRpm(spindle_rpm) });

	for tool_path in tool_paths {
		if let Some(start_point) = tool_path.get_point(0) {
			let move_to_start_point = match current_position {
				Some(c_p) => c_p != start_point,
				None => true,
			};

			if move_to_start_point {
				// Move up
				commands.append(&mut cnc_move_up(current_z_position, safe_height, move_height, feedrate_move_z, feedrate_plunge));
				current_z_position = move_height;
				// Move to start point
				commands.push(GcodeCommand::Move {
					t: MoveType::Rapid,
					x: Some(start_point.x),
					y: Some(start_point.y),
					z: None,
					feedrate: feedrate_move,
				});
			}
			current_position = Some(start_point);

			let mut reversed = false;

			for z_index in 1..=z_steps {
				let points = tool_path.get_point_iter();
				let points = if reversed { itertools::Either::Left(points.rev()) } else { itertools::Either::Right(points) };

				let pass_z_height = (-z_step * z_index as FT).max(z_depth);
				// Move down
				commands.append(&mut cnc_move_down(current_z_position, safe_height, pass_z_height, feedrate_move_z, feedrate_plunge));
				current_z_position = pass_z_height;

				for point in points {
					if *current_position.unwrap() == *point {
						continue;
					}

					commands.push(GcodeCommand::Move {
						t: MoveType::Tool,
						x: Some(point.x),
						y: Some(point.y),
						z: None,
						feedrate: feedrate_work,
					});
					current_position = Some(point);
				}

				reversed = !reversed;
			}
		}
	}
	// Move up
	commands.append(&mut cnc_move_up(current_z_position, safe_height, move_height, feedrate_move_z, feedrate_plunge));
	commands.push(GcodeCommand::ToolOff);

	commands
}

pub fn cnc_move_up(current_z: FT, safe_height: FT, move_height: FT, feedrate_move: u32, feedrate_plunge: u32) -> Vec<GcodeCommand> {
	let mut commands = Vec::new();
	if current_z < safe_height {
		commands.push(GcodeCommand::Move {
			t: MoveType::Tool,
			x: None,
			y: None,
			z: Some(safe_height),
			feedrate: feedrate_plunge,
		});
	}
	commands.push(GcodeCommand::Move {
		t: MoveType::Rapid,
		x: None,
		y: None,
		z: Some(move_height),
		feedrate: feedrate_move,
	});
	commands
}

pub fn cnc_move_down(current_z: FT, safe_height: FT, target_depth: FT, feedrate_move: u32, feedrate_plunge: u32) -> Vec<GcodeCommand> {
	let mut commands = Vec::new();
	if current_z > safe_height {
		commands.push(GcodeCommand::Move {
			t: MoveType::Rapid,
			x: None,
			y: None,
			z: Some(safe_height),
			feedrate: feedrate_move,
		});
	}
	commands.push(GcodeCommand::Move {
		t: MoveType::Tool,
		x: None,
		y: None,
		z: Some(target_depth),
		feedrate: feedrate_plunge,
	});
	commands
}

pub fn generate_drill_commands(feedrate_move: u32, feedrate_move_z: u32, feedrate_plunge: u32, z_depth: FT, move_height: FT, safe_height: FT, spindle_rpm: u32, holes: &[Point]) -> Vec<GcodeCommand> {
	assert!(z_depth < 0.0);

	let mut commands = Vec::new();
	commands.push(GcodeCommand::Move {
		t: MoveType::Tool,
		x: None,
		y: None,
		z: Some(move_height),
		feedrate: feedrate_move_z,
	});
	commands.push(GcodeCommand::ToolOn { power: ToolPower::SpindleRpm(spindle_rpm) });

	for hole in holes {
		let position = hole;
		commands.push(GcodeCommand::Move {
			t: MoveType::Rapid,
			x: Some(position.x),
			y: Some(position.y),
			z: None,
			feedrate: feedrate_move,
		});
		// Drill
		commands.append(&mut cnc_move_down(move_height, safe_height, z_depth, feedrate_move_z, feedrate_plunge));
		// Move up
		commands.append(&mut cnc_move_up(z_depth, safe_height, move_height, feedrate_move_z, feedrate_plunge));
	}

	commands.push(GcodeCommand::ToolOff);

	commands
}

pub fn generate_drillmill_commands(
	feedrate_move: u32,
	feedrate_move_z: u32,
	feedrate_work: u32,
	feedrate_plunge: u32,
	z_depth: FT,
	z_step: FT,
	move_height: FT,
	safe_height: FT,
	spindle_rpm: u32,
	mill_diameter: FT,
	holes: &[Hole],
) -> Vec<GcodeCommand> {
	assert!(z_depth < 0.0);
	let z_steps = (z_depth / z_step).abs().ceil() as usize;

	let mut commands = Vec::new();
	commands.push(GcodeCommand::Move {
		t: MoveType::Tool,
		x: None,
		y: None,
		z: Some(move_height),
		feedrate: feedrate_move_z,
	});
	commands.push(GcodeCommand::ToolOn { power: ToolPower::SpindleRpm(spindle_rpm) });

	for hole in holes {
		let center = hole.get_position();
		let diameter = hole.get_type().diameter();
		let radius = diameter / 2.0;

		if diameter < mill_diameter {
			print_warning(&format!("Hole at {} is too small to be milled (diameter: {})", center, diameter));
			continue;
		}

		let start_pos_offset = Point::new(radius - mill_diameter / 2.0, 0.0);

		let start_pos = Translation2::from(start_pos_offset) * center;

		commands.push(GcodeCommand::Move {
			t: MoveType::Rapid,
			x: Some(start_pos.x),
			y: Some(start_pos.y),
			z: None,
			feedrate: feedrate_move,
		});

		let mut current_z_position = move_height;
		// Mill
		for z_index in 1..=z_steps {
			let pass_z_height = (-z_step * z_index as FT).max(z_depth);
			commands.append(&mut cnc_move_down(current_z_position, safe_height, pass_z_height, feedrate_move_z, feedrate_plunge));
			current_z_position = pass_z_height;

			commands.push(GcodeCommand::MoveArc {
				ccw: true,
				center_offset: -start_pos_offset,
				end_pos: None,
				feedrate: feedrate_work,
			});
		}
		// Move up
		commands.append(&mut cnc_move_up(current_z_position, safe_height, move_height, feedrate_move_z, feedrate_plunge));
	}

	commands.push(GcodeCommand::ToolOff);

	commands
}

pub fn commands_calculate_stats(commands: &[GcodeCommand]) -> GcodeStats {
	let mut min_x = INFINITY;
	let mut min_y = INFINITY;
	let mut max_x = NEG_INFINITY;
	let mut max_y = NEG_INFINITY;

	let mut last_position = Point3::new(0.0, 0.0, 0.0);
	let mut duration = Duration::from_secs_f64(0.0);

	for c in commands {
		// Duration
		duration += c.get_duration(last_position);

		if let GcodeCommand::Move { t: _, x, y, z, feedrate: _ } = c {
			// Dimensions
			if let Some(x) = x {
				if *x < min_x {
					min_x = *x;
				}
				if *x > max_x {
					max_x = *x;
				}
			}
			if let Some(y) = y {
				if *y < min_y {
					min_y = *y;
				}
				if *y > max_y {
					max_y = *y;
				}
			}

			last_position = Point3::new(x.unwrap_or(last_position.x), y.unwrap_or(last_position.y), z.unwrap_or(last_position.z));
		}
	}

	let line_num = commands.len() as u64;

	GcodeStats {
		min_p: Point::new(min_x, min_y),
		max_p: Point::new(max_x, max_y),
		line_count: line_num,
		duration,
	}
}

pub struct GcodeStats {
	pub min_p: Point,
	pub max_p: Point,
	pub line_count: u64,
	pub duration: Duration,
}
