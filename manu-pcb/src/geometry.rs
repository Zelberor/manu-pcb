// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{cmp::Ordering, f64::consts::PI};

use approx::abs_diff_eq;
use dxf;
use na::Point3 as P3;
use na::{distance, Matrix2, Point2, Rotation2, Translation2, UnitVector2, Vector2};
use nalgebra as na;

const ARC_SEGMENT_ANGLE: FT = 2.0 / 180.0 * PI;
const KICAD_ROUNDED_EQ_DECIMALS: u32 = 3;

pub trait IntersectsWith<T> {
	type Intersections;

	/// Returns the intersections between two shapes
	///
	/// If returning a list all elements are expected to be unique.
	/// Coincident sections also must be unique (No other intersection points on the coincident section)
	fn intersects_with(&self, t: &T) -> Self::Intersections;
}

pub trait DistanceTo<T, FT> {
	fn distance_to(&self, other: &T) -> FT;
}

pub trait KicadRoundedEqCmp {
	fn kicad_roughly_eq_cmp(&self, other: &Self) -> Ordering;
}

pub trait TotalCmp {
	fn total_cmp(&self, other: &Self) -> Ordering;
}

pub type FT = f64;
pub type Point = Point2<FT>;
pub type Point3 = P3<FT>;

#[derive(Debug, Clone, Copy)]
pub struct Line2D {
	pub p1: Point,
	pub p2: Point,
}

impl From<&dxf::entities::Line> for Line2D {
	fn from(value: &dxf::entities::Line) -> Self {
		Line2D {
			p1: Point2::new(value.p1.x as FT, value.p1.y as FT),
			p2: Point2::new(value.p2.x as FT, value.p2.y as FT),
		}
	}
}

impl Line2D {
	pub fn new(p1: Point, p2: Point) -> Self {
		Line2D { p1, p2 }
	}

	pub fn move_point_rangle(&self, p: Point, distance: FT) -> Point {
		let line_dir = self.get_direction();
		let dir_90 = Rotation2::new(PI / 2.0) * line_dir;
		Translation2::from(dir_90.into_inner() * distance) * p
	}

	pub fn parallel(&self, distance: FT) -> Self {
		Line2D {
			p1: self.move_point_rangle(self.p1, distance),
			p2: self.move_point_rangle(self.p2, distance),
		}
	}

	pub fn get_direction(&self) -> UnitVector2<FT> {
		let line_dir = self.p2.coords - self.p1.coords;
		UnitVector2::new_normalize(line_dir)
	}

	pub fn from_arc(arc: &dxf::entities::Arc) -> Vec<Self> {
		let mut lines = Vec::new();

		let base_dir = UnitVector2::new_unchecked(Vector2::new(1.0, 0.0));
		let start_angle = arc.start_angle / 180.0 * PI;
		let end_angle = arc.end_angle / 180.0 * PI;
		let angle = end_angle - start_angle;
		let center = Point::new(arc.center.x as FT, arc.center.y as FT);
		// TODO: Segment angle based on radius so size stays constant
		let segment_num = (angle / ARC_SEGMENT_ANGLE).abs().ceil() as usize;
		let segment_angle = angle / (segment_num as FT);
		for i in 0..segment_num {
			let segment_start_a = start_angle + segment_angle * (i as FT);
			let segment_end_a = start_angle + segment_angle * ((i + 1) as FT);
			let segment_start_dir = Rotation2::new(segment_start_a) * base_dir;
			let segment_start_p = Translation2::from(segment_start_dir.into_inner() * arc.radius) * center;
			let segment_end_dir = Rotation2::new(segment_end_a) * base_dir;
			let segment_end_p = Translation2::from(segment_end_dir.into_inner() * arc.radius) * center;
			lines.push(Line2D::new(segment_start_p, segment_end_p));
		}
		lines
	}

	pub fn from_circle(circle: &dxf::entities::Circle) -> Vec<Self> {
		let circle_arc = dxf::entities::Arc::new(circle.center.clone(), circle.radius, 0.0, 360.0);
		Line2D::from_arc(&circle_arc)
	}

	pub fn length(&self) -> FT {
		distance(&self.p1, &self.p2)
	}

	pub fn get_middle(&self) -> Point {
		Translation2::from(self.get_direction().into_inner() * self.length() * 0.5) * self.p1
	}
}

impl DistanceTo<Point, FT> for Line2D {
	fn distance_to(&self, other: &Point) -> FT {
		let line_dir = self.get_direction();
		let p1_p_dir = Line2D::new(self.p1, *other).get_direction();

		let signed_angle = -(line_dir.x * p1_p_dir.y - line_dir.y * p1_p_dir.x).atan2(line_dir.x * p1_p_dir.x + line_dir.y * p1_p_dir.y);

		let p1_p_distance = distance(&self.p1, &other);

		p1_p_distance * signed_angle.sin()
	}
}

impl DistanceTo<Line2D, FT> for Point {
	fn distance_to(&self, other: &Line2D) -> FT {
		other.distance_to(self)
	}
}

impl KicadRoundedEqCmp for Line2D {
	fn kicad_roughly_eq_cmp(&self, other: &Self) -> Ordering {
		match self.p1.kicad_roughly_eq_cmp(&other.p1) {
			Ordering::Equal => self.p2.kicad_roughly_eq_cmp(&other.p2),
			ord => ord,
		}
	}
}

pub enum LineLineIntersection {
	Point { p: Point, inside_bounds: bool },
	Coincident { p1: Point, p2: Point, overlaps: bool },
	Parallel,
}

impl IntersectsWith<Line2D> for Line2D {
	type Intersections = LineLineIntersection;
	fn intersects_with(&self, line: &Line2D) -> Self::Intersections {
		// Based on https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#Given_two_points_on_each_line_segment
		let x1 = self.p1.x;
		let x2 = self.p2.x;
		let y1 = self.p1.y;
		let y2 = self.p2.y;

		let x3 = line.p1.x;
		let x4 = line.p2.x;
		let y3 = line.p1.y;
		let y4 = line.p2.y;

		let divisor = Matrix2::new(x1 - x2, x3 - x4, y1 - y2, y3 - y4).determinant();

		if abs_diff_eq!(divisor, 0.0) {
			if abs_diff_eq!(self.distance_to(&line.p1), 0.0) {
				let mut points = [self.p1, self.p2, line.p1, line.p2];
				points.sort_by(|a, b| a.total_cmp(b));
				let overlaps = distance(&points[0], &points[3]) < self.length() + line.length();
				LineLineIntersection::Coincident { p1: points[1], p2: points[2], overlaps }
			} else {
				LineLineIntersection::Parallel
			}
		} else {
			let t_dividend = Matrix2::new(x1 - x3, x3 - x4, y1 - y3, y3 - y4).determinant();
			let u_dividend = Matrix2::new(x1 - x3, x1 - x2, y1 - y3, y1 - y2).determinant();
			let dividends = [t_dividend, u_dividend];
			let mut in_bounds = true;

			for d in dividends {
				if !(abs_diff_eq!(d, 0.0) || (d.signum() == divisor.signum() && d.abs() <= divisor.abs())) {
					in_bounds = false;
				}
			}
			let t = t_dividend / divisor;

			let p_x = x1 + t * (x2 - x1);
			let p_y = y1 + t * (y2 - y1);
			LineLineIntersection::Point {
				p: Point::new(p_x, p_y),
				inside_bounds: in_bounds,
			}
		}
	}
}

pub trait CmpRefDistance {
	fn cmp_ref_distance(&self, other: &Self, reference: &Self) -> Ordering;
}

impl CmpRefDistance for Point {
	fn cmp_ref_distance(&self, other: &Self, reference: &Self) -> Ordering {
		let dist1 = distance(self, reference);
		let dist2 = distance(other, reference);
		dist1.total_cmp(&dist2)
	}
}

impl KicadRoundedEqCmp for Point {
	fn kicad_roughly_eq_cmp(&self, other: &Self) -> Ordering {
		if stupid_round(self.x, KICAD_ROUNDED_EQ_DECIMALS) == stupid_round(other.x, KICAD_ROUNDED_EQ_DECIMALS) {
			if stupid_round(self.y, KICAD_ROUNDED_EQ_DECIMALS) == stupid_round(other.y, KICAD_ROUNDED_EQ_DECIMALS) {
				Ordering::Equal
			} else if self.y < other.y {
				Ordering::Less
			} else {
				Ordering::Greater
			}
		} else if self.x < other.x {
			Ordering::Less
		} else {
			Ordering::Greater
		}
	}
}

impl TotalCmp for Point {
	fn total_cmp(&self, other: &Self) -> Ordering {
		match self.x.total_cmp(&other.x) {
			Ordering::Equal => self.y.total_cmp(&other.y),
			ord => ord,
		}
	}
}

fn stupid_round(x: FT, decimals: u32) -> i64 {
	let y = 10i64.pow(decimals) as f64;
	(x * y).round() as i64
}

#[cfg(test)]
mod tests {
	use approx::assert_abs_diff_eq;

	use crate::geometry::{DistanceTo, Line2D, Point};

	#[test]
	fn line_point_distance_positive() {
		let line = Line2D::new(Point::new(0.0, 0.0), Point::new(0.0, 2.0));
		let point = Point::new(1.0, 1.0);
		let distance = line.distance_to(&point);
		assert_abs_diff_eq!(distance, 1.0);
	}

	#[test]
	fn line_point_distance_negative() {
		let line = Line2D::new(Point::new(0.0, 0.0), Point::new(0.0, 2.0));
		let point = Point::new(-1.0, 1.0);
		let distance = line.distance_to(&point);
		assert_abs_diff_eq!(distance, -1.0);
	}
}
