// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![allow(dead_code)]

mod drill;
mod gcode;
mod geometry;
mod line_filler;
mod line_tracer;
mod outline;
mod outline_tracer;
mod print;
mod toolpath;

use clap::arg;
use clap::command;
use clap::Args;
use clap::Parser;
use clap::Subcommand;
use clap::ValueEnum;
use dxf::entities::*;
use dxf::Drawing;
use dxf::DxfError;
use gcode::flavour::FlavourError;
use gcode::flavour::GcodeWriteError;
use gcode::GcodeCommand;
use geometry::FT;
use line_filler::LineFiller;
use nalgebra::Rotation2;
use nalgebra::Transform2;
use nalgebra::Translation2;
use outline::Outline;
use outline::OutlineError;
use outline_tracer::OutlineTracer;
use outline_tracer::OutlineTracerError;
use std::collections::HashMap;
use std::f64::consts::PI;
use std::fmt::Display;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Error as IoError;
use std::path::Path;
use std::path::PathBuf;
use std::process::exit;

use crate::gcode::commands_calculate_stats;
use crate::gcode::flavour::FlavourWrite;
use crate::gcode::ToolType;
use crate::geometry::Line2D;
use crate::geometry::Point;
use crate::print::print_warning;
use crate::toolpath::ToolLine;
use crate::toolpath::ToolPath;

#[derive(Debug)]
enum Error {
	DxfError(DxfError),
	OutlineError(OutlineError),
	OutlineTracerError(OutlineTracerError),
	MultipleEdges,
	NoEdge,
	IoError(IoError),
	FlavourError(FlavourError),
	DrillFileParseError(drill::FileParseError),
}

impl From<DxfError> for Error {
	fn from(value: DxfError) -> Self {
		Error::DxfError(value)
	}
}

impl From<OutlineError> for Error {
	fn from(value: OutlineError) -> Self {
		Error::OutlineError(value)
	}
}

impl From<OutlineTracerError> for Error {
	fn from(value: OutlineTracerError) -> Self {
		Error::OutlineTracerError(value)
	}
}

impl From<IoError> for Error {
	fn from(value: IoError) -> Self {
		Error::IoError(value)
	}
}

impl From<FlavourError> for Error {
	fn from(value: FlavourError) -> Self {
		Error::FlavourError(value)
	}
}

impl From<GcodeWriteError> for Error {
	fn from(value: GcodeWriteError) -> Self {
		match value {
			GcodeWriteError::IoError(e) => e.into(),
			GcodeWriteError::FlavourError(e) => e.into(),
		}
	}
}

impl From<drill::Error> for Error {
	fn from(value: drill::Error) -> Self {
		match value {
			drill::Error::IoError(e) => Error::IoError(e),
			drill::Error::ParseError(e) => Error::DrillFileParseError(e),
		}
	}
}

fn main() {
	let args = Cli::parse();

	let result = main_handled(args);

	if let Err(err) = result {
		println!("Error: {:?}", err);
		exit(1)
	}
}

fn main_handled(args: Cli) -> Result<(), Error> {
	let mut edge = load_dxf_into_outline(&args.config.edge)?;
	if edge.len() > 1 {
		return Err(Error::MultipleEdges);
	}
	let mut edge = match edge.pop() {
		Some(e) => e,
		None => return Err(Error::NoEdge),
	};

	// Apply rotation
	let rotation = Rotation2::new(if let Some(r) = args.config.rotation.rotation_degrees {
		r / 180.0 * PI
	} else if let Some(r) = args.config.rotation.rotation_radians {
		r
	} else {
		0.0
	});
	let rot_transform = Transform2::from_matrix_unchecked(rotation.to_homogeneous());
	edge.transform(rot_transform);

	// Make sure min of the bounding_box is (0,0)
	let bounding_rect = edge.get_bounding_rect();
	let translation = Translation2::from(-bounding_rect.0.coords);
	let transl_transform = Transform2::from_matrix_unchecked(translation.to_homogeneous());
	edge.transform(transl_transform);

	let pre_transform = transl_transform * rot_transform;
	let translation = Translation2::new(args.config.offset_x.unwrap_or(0.0), args.config.offset_y.unwrap_or(0.0));
	let post_transform = Transform2::from_matrix_unchecked(translation.to_homogeneous());

	match &args.command {
		Command::GenerateLaser { config, input } => generate_laser_gcode(input, &args.config, config, edge, pre_transform, post_transform),
		Command::GenerateDrill { drill, input, cnc } => generate_drill_gcode(input, &args.config, cnc, drill, pre_transform, post_transform),
		Command::GenerateMill { mill, cnc } => generate_mill_gcode(&args.config, cnc, mill, edge, post_transform),
		Command::GenerateDrillMill { input, cnc, mill, min_hole_diameter } => generate_drillmill_gcode(input, *min_hole_diameter, &args.config, cnc, mill, pre_transform, post_transform),
		Command::GenerateAlignmentHole { drill, cnc } => generate_alignment_hole(&args.config, cnc, drill),
	}
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
	#[command(flatten)]
	config: GeneralConfig,

	#[command(subcommand)]
	command: Command,
}

#[derive(Subcommand)]
enum Command {
	GenerateLaser {
		/// Path to the layer outlines file in DXF format
		#[arg(short, long)]
		input: PathBuf,
		#[command(flatten)]
		config: LaserConfig,
	},
	GenerateDrill {
		/// Path to the drill file
		#[arg(short, long)]
		input: PathBuf,
		#[command(flatten)]
		cnc: CncConfig,
		#[command(flatten)]
		drill: DrillConfig,
	},
	/// Drills holes by milling them
	GenerateDrillMill {
		/// Path to the drill file
		#[arg(short, long)]
		input: PathBuf,
		#[arg(long)]
		min_hole_diameter: FT,
		#[command(flatten)]
		cnc: CncConfig,
		#[command(flatten)]
		mill: MillConfig,
	},
	GenerateMill {
		#[command(flatten)]
		cnc: CncConfig,
		#[command(flatten)]
		mill: MillConfig,
	},
	/// Drill an alignment hole at position (0.0, 0.0).
	/// Ignores offsets and rotation
	GenerateAlignmentHole {
		#[command(flatten)]
		cnc: CncConfig,
		#[command(flatten)]
		drill: DrillConfig,
	},
}

#[derive(Args)]
#[group(required = false, multiple = false)]
struct Rotation {
	#[arg(short = 'r', long)]
	rotation_degrees: Option<FT>,
	#[arg(long)]
	rotation_radians: Option<FT>,
}

#[derive(Args)]
struct LaserConfig {
	/// Invert what areas are engraved. Use it when generating the gcode to laser free the pads
	#[arg(short = 'v', long, default_value_t = false)]
	inverted: bool,

	#[arg(short = 'd', long)]
	diameter: FT,
	#[arg(short = 's', long)]
	spacing: FT,
	#[arg(short = 'n', long, default_value_t = 2)]
	passes: usize,
	#[arg(short = 'p', long, default_value_t = 1.0)]
	power: f32,

	/// Feedrate in mm/min when laser is line-filling areas
	#[arg(short = 'w', long, default_value_t = 2000)]
	feedrate_linefill: u32,

	/// Shortens the toolpaths before the laser is turned off
	#[arg(long)]
	overshoot_compensation_linefill: Option<FT>,

	/// Toolhead waits for specified amount of time in ms before turning the laser on.
	/// Avoids overshoot from previous move
	#[arg(long)]
	dwell_before_on: Option<u64>,

	/// If the laser should trace the outlines of the shapes of the layer
	#[arg(long, default_value_t = true)]
	trace_outlines: bool,
	/// Feedrate in mm/min when laser is tracing outlines
	#[arg(long, default_value_t = 500)]
	feedrate_trace: u32,
}

#[derive(Args)]
struct CncConfig {
	/// Move height
	#[arg(long, default_value_t = 15.0)]
	move_height: FT,
	/// Safe height (Height the cnc moves to before starting to mill or drill)
	#[arg(long, default_value_t = 1.0)]
	safe_height: FT,
	/// Milling/Drilling depth
	#[arg(short = 'z', long)]
	z_depth: FT,
}

#[derive(Args)]
struct MillConfig {
	#[arg(short = 'd', long)]
	diameter: FT,

	/// Feedrate in mm/min when milling
	#[arg(short = 'w', long, default_value_t = 100)]
	feedrate_work: u32,
	/// Feedrate in mm/min when plunging
	#[arg(long, default_value_t = 30)]
	feedrate_plunge: u32,

	#[arg(short = 'p', long)]
	rpm: u32,

	/// Milling step height for each pass
	#[arg(short = 's', long)]
	z_step: FT,

	/// Insert a tab every _ mm
	#[arg(long)]
	tabs_every: Option<FT>,
	/// Tab width
	#[arg(long, default_value_t = 1.0)]
	tab_width: FT,
	/// Tab start position offset
	#[arg(long)]
	tab_offset: Option<FT>,
}

#[derive(Args)]
struct DrillConfig {
	/// Feedrate in mm/min when drilling
	#[arg(short = 'w', long, default_value_t = 30)]
	feedrate_plunge: u32,

	#[arg(short = 'p', long)]
	rpm: u32,
}

#[derive(Args)]
struct GeneralConfig {
	/// Path to the edge of the pcb in DXF format. Must be a single outline
	#[arg(short, long)]
	edge: PathBuf,

	#[arg(long, default_value_t = false)]
	mirror_x: bool,
	#[arg(long, default_value_t = false)]
	mirror_y: bool,

	#[arg(short = 'x', long)]
	offset_x: Option<FT>,
	#[arg(short = 'y', long)]
	offset_y: Option<FT>,

	#[command(flatten)]
	rotation: Rotation,

	#[arg(short = 'f', long, default_value_t = Flavour::Generic)]
	flavour: Flavour,

	/// Output gcode file path
	#[arg(short, long)]
	output: PathBuf,

	/// Feedrate in mm/min when moving on x/y axis
	#[arg(short = 'm', long, default_value_t = 3000)]
	feedrate_move: u32,
	/// Feedrate in mm/min when moving on z axis
	#[arg(long, default_value_t = 600)]
	feedrate_move_z: u32,
}

#[derive(ValueEnum, Clone, Copy)]
enum Flavour {
	Generic,
	Snapmaker,
}

impl Display for Flavour {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Flavour::Generic => write!(f, "generic"),
			Flavour::Snapmaker => write!(f, "snapmaker"),
		}
	}
}

fn generate_laser_gcode(input: &Path, general: &GeneralConfig, laser: &LaserConfig, edge: Outline, pre_transform: Transform2<FT>, post_transform: Transform2<FT>) -> Result<(), Error> {
	let mut layer = load_dxf_into_outline(input)?;
	for out in &mut layer {
		out.transform(pre_transform);
	}
	let bounding_rect = edge.get_bounding_rect();
	assert_eq!(bounding_rect.0, Point::new(0.0, 0.0), "Min point expected to be (0,0)");

	let edges = [edge];

	// Fill
	let offset = if !laser.trace_outlines { laser.diameter * 0.5 } else { laser.diameter * 0.75 };
	let mut tool_lines: Vec<Vec<ToolLine>> = Vec::new();
	let pass_angle = PI / laser.passes as FT;
	for pass_idx in 0..laser.passes {
		let angle = pass_idx as f64 * pass_angle;
		let filler = LineFiller::new(angle, bounding_rect.1.coords, offset, laser.spacing);
		tool_lines.append(&mut filler.create_tool_lines(&layer, &edges, laser.inverted))
	}
	let mut tool_paths: Vec<Vec<ToolPath>> = tool_lines.iter().map(|l| l.iter().map(|l| ToolPath::from(l)).collect()).collect();

	if laser.trace_outlines {
		// Outline tracing
		let tracer = OutlineTracer::new(laser.diameter * 0.5);
		let mut traced_paths = Vec::new();
		for o in layer.iter() {
			match tracer.create_tool_path(o, &layer, laser.inverted) {
				Ok(t) => traced_paths.push(t),
				Err(e) => print_warning(&format!("Could not trace outline: {:?}", e)),
			}
		}
		tool_paths.push(traced_paths)
	}

	for list in tool_paths.iter_mut() {
		for path in list {
			path.transform(post_transform);
			if general.mirror_x {
				path.mirror_x();
			}
			if general.mirror_y {
				path.mirror_y();
			}
		}
	}
	let mut commands = Vec::new();

	if laser.trace_outlines {
		let traced_paths = tool_paths.pop().unwrap();
		let mut c = gcode::generate_laser_commands(general.feedrate_move, laser.feedrate_trace, laser.power, &traced_paths, false, None, laser.dwell_before_on);
		commands.append(&mut c);
	}

	let mut reversed = false;
	for list in tool_paths {
		let mut c = gcode::generate_laser_commands(general.feedrate_move, laser.feedrate_linefill, laser.power, &list, reversed, laser.overshoot_compensation_linefill, laser.dwell_before_on);
		commands.append(&mut c);
		reversed = !reversed;
	}

	write_gcode_file(&general.output, general.flavour, &commands, ToolType::Laser, Some(&bounding_rect))?;

	Ok(())
}

fn load_dxf_into_outline(path: &Path) -> Result<Vec<Outline>, Error> {
	let mut lines = Vec::new();

	let drawing = Drawing::load_file(path)?;
	for e in drawing.entities() {
		match &e.specific {
			EntityType::Line(ref line) => {
				let line2d: Line2D = line.into();
				lines.push(line2d);
			}
			EntityType::Arc(ref arc) => {
				let mut arc_lines = Line2D::from_arc(arc);
				lines.append(&mut arc_lines);
			}
			EntityType::Circle(ref circle) => {
				let mut circle_lines = Line2D::from_circle(circle);
				lines.append(&mut circle_lines);
			}
			e => println!("Warning: Unsupported entity: {:?}", e),
		}
	}

	Ok(Outline::from_lines(lines.iter())?)
}

fn generate_mill_gcode(general: &GeneralConfig, cnc: &CncConfig, mill: &MillConfig, edge: Outline, post_transform: Transform2<FT>) -> Result<(), Error> {
	let tracer = OutlineTracer::new(mill.diameter / 2.0);
	let edges = [edge];

	let mut tool_path = tracer.create_tool_path(&edges[0], &edges, false)?;
	tool_path.transform(post_transform);
	if general.mirror_x {
		tool_path.mirror_x();
	}
	if general.mirror_y {
		tool_path.mirror_y();
	}

	let tool_paths = match mill.tabs_every {
		Some(i) => {
			if let Some(offset) = mill.tab_offset {
				tool_path = tool_path.circular_move_start(offset);
			}
			let mut split_paths = tool_path.split_every(i);
			let mut tool_paths = Vec::new();
			while let Some(path) = split_paths.pop() {
				let s_path = path.shorten_ends((mill.tab_width + mill.diameter) / 2.0);
				if let Some(path) = s_path {
					tool_paths.push(path);
				}
			}
			tool_paths
		}
		None => vec![tool_path],
	};

	let commands = gcode::generate_mill_commands(
		general.feedrate_move,
		general.feedrate_move_z,
		mill.feedrate_work,
		mill.feedrate_plunge,
		cnc.z_depth,
		mill.z_step,
		cnc.move_height,
		cnc.safe_height,
		mill.rpm,
		&tool_paths,
	);
	write_gcode_file(&general.output, general.flavour, &commands, ToolType::Cnc, None)?;

	Ok(())
}

fn generate_drill_gcode(input: &Path, general: &GeneralConfig, cnc: &CncConfig, drill: &DrillConfig, pre_transform: Transform2<FT>, post_transform: Transform2<FT>) -> Result<(), Error> {
	let drill_file = File::open(input)?;
	let buf_reader = BufReader::new(drill_file);

	let holes = drill::load_drill_file(buf_reader)?;
	let mut hole_positions = HashMap::new();
	for mut hole in holes {
		let hole_type = hole.get_type();
		let diameter = format!("{:.4}", hole_type.diameter());

		let point_list = match hole_positions.get_mut(&diameter) {
			Some(l) => l,
			None => {
				hole_positions.insert(diameter.clone(), Vec::new());
				hole_positions.get_mut(&diameter).unwrap()
			}
		};

		hole.transform(pre_transform);
		hole.transform(post_transform);
		if general.mirror_x {
			hole.mirror_x()
		}
		if general.mirror_y {
			hole.mirror_y()
		}

		point_list.push(hole.get_position());
	}

	let extension = general.output.extension();

	for (k, positions) in hole_positions {
		let commands = gcode::generate_drill_commands(general.feedrate_move, general.feedrate_move_z, drill.feedrate_plunge, cnc.z_depth, cnc.move_height, cnc.safe_height, drill.rpm, &positions);
		let mut output = general.output.clone();
		output.set_extension(match extension {
			Some(e) => format!("{}.{}", k, e.to_str().expect("File path contains invalid characters.")),
			None => format!("{}.nc", k),
		});
		write_gcode_file(&output, general.flavour, &commands, ToolType::Cnc, None)?;
	}

	Ok(())
}

fn generate_drillmill_gcode(input: &Path, min_hole_diameter: FT, general: &GeneralConfig, cnc: &CncConfig, mill: &MillConfig, pre_transform: Transform2<FT>, post_transform: Transform2<FT>) -> Result<(), Error> {
	let drill_file = File::open(input)?;
	let buf_reader = BufReader::new(drill_file);

	let holes = drill::load_drill_file(buf_reader)?;
	let holes: Vec<_> = holes
		.iter()
		.cloned()
		.filter_map(|mut hole| {
			let hole_type = hole.get_type();
			if hole_type.diameter() < min_hole_diameter {
				return None;
			}

			hole.transform(pre_transform);
			hole.transform(post_transform);
			if general.mirror_x {
				hole.mirror_x()
			}
			if general.mirror_y {
				hole.mirror_y()
			}
			Some(hole)
		})
		.collect();

	let commands = gcode::generate_drillmill_commands(
		general.feedrate_move,
		general.feedrate_move_z,
		mill.feedrate_work,
		mill.feedrate_plunge,
		cnc.z_depth,
		mill.z_step,
		cnc.move_height,
		cnc.safe_height,
		mill.rpm,
		mill.diameter,
		&holes,
	);
	write_gcode_file(&general.output, general.flavour, &commands, ToolType::Cnc, None)?;

	Ok(())
}

fn generate_alignment_hole(general: &GeneralConfig, cnc: &CncConfig, drill: &DrillConfig) -> Result<(), Error> {
	let commands = gcode::generate_drill_commands(
		general.feedrate_move,
		general.feedrate_move_z,
		drill.feedrate_plunge,
		cnc.z_depth,
		cnc.move_height,
		cnc.safe_height,
		drill.rpm,
		&[Point::new(0.0, 0.0)],
	);
	write_gcode_file(&general.output, general.flavour, &commands, ToolType::Cnc, None)?;

	Ok(())
}

fn write_gcode_file(path: &Path, flavour: Flavour, commands: &[GcodeCommand], tool_type: ToolType, bounding_rect: Option<&(Point, Point)>) -> Result<(), Error> {
	let mut stats = commands_calculate_stats(&commands);
	// Todo: Fix bounding rect
	/* if let Some(b_rect) = bounding_rect {
		stats.min_p = b_rect.0;
		stats.max_p = b_rect.1;
	} */

	println!("Duration: {}", humantime::Duration::from(stats.duration));

	let f = File::create(path)?;
	let mut file_writer = BufWriter::new(f);

	match flavour {
		Flavour::Generic => {
			let writer = gcode::flavour::Generic {};
			writer.write_gcode(&mut file_writer, &commands, &stats)?;
		}
		Flavour::Snapmaker => {
			let writer = gcode::flavour::snapmaker::Snapmaker::new(tool_type);
			writer.write_gcode(&mut file_writer, &commands, &stats)?;
		}
	}

	Ok(())
}
