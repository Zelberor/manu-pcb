// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use na::distance;
use na::Rotation2;
use na::Translation2;
use na::Unit;
use na::Vector2;
use nalgebra as na;

use std::cmp::Ordering;
use std::f64::consts::PI;

use crate::geometry::CmpRefDistance;
use crate::geometry::Line2D;
use crate::geometry::Point;
use crate::geometry::FT;
use crate::line_tracer::LineTracer;
use crate::outline::Outline;
use crate::toolpath::ToolLine;

pub struct LineFiller {
	/// Dimensions of a rect around the pcb (width, height)
	dimensions: Vector2<FT>,
	/// Offset to all outlines. "Radius" of the tool
	offset: FT,
	/// Line spacing
	spacing: FT,
	/// Line angle in radians; 0 = horizontal; rotates counter-clockwise
	angle: FT,
	/// Direction vector of the lines; calculated from angle
	direction: Unit<Vector2<FT>>,
	/// width of the field being filled (90 deg to direction)
	rake_width: FT,
	trace_length: FT,
}

impl LineFiller {
	pub fn new(angle: FT, dimensions: Vector2<FT>, offset: FT, spacing: FT) -> Self {
		let base_dir = Unit::new_normalize(Vector2::new(1.0, 0.0));
		let direction = Rotation2::new(angle) * base_dir;
		let rake_width = project_line_on_rect(dimensions, angle + PI * 0.5);
		let trace_length = project_line_on_rect(dimensions, angle);

		LineFiller {
			dimensions,
			offset,
			spacing,
			angle,
			direction,
			rake_width,
			trace_length,
		}
	}

	pub fn create_tool_lines(&self, outlines: &[Outline], edges: &[Outline], invert: bool) -> Vec<Vec<ToolLine>> {
		// TODO: sanity check that no point from outline is outside of dimensions

		let mut paths = Vec::new();

		let dir_90 = Rotation2::new(PI / 2.0) * self.direction;

		let line_num_half = (self.rake_width / self.spacing / 2.0).ceil() as i64;
		for line_num in -line_num_half..=line_num_half {
			let offset = line_num as FT * self.spacing;

			let center: Point = (self.dimensions * 0.5).into();

			let p1 = Translation2::from(dir_90.into_inner() * offset) * center;
			let p2 = Translation2::from(self.direction.into_inner() * self.trace_length * 0.5) * p1;
			let p1 = Translation2::from(-self.direction.into_inner() * self.trace_length * 0.5) * p1;
			let middle_line = Line2D::new(p1, p2);

			// the LineFiller traces two lines at the edges of the toolpath
			let mut line_calcs = [
				LineTracer::new(middle_line, -self.offset, outlines.iter(), !invert),
				LineTracer::new(middle_line, self.offset, outlines.iter(), !invert),
				LineTracer::new(middle_line, -self.offset, edges.iter(), false),
				LineTracer::new(middle_line, self.offset, edges.iter(), false),
			];

			let mut tool_lines = Vec::new();
			let mut last_point = p1;
			loop {
				let laser_enabled = line_calcs.iter().map(|l| l.get_laser_enabled()).fold(true, |a, b| a && b);
				let mut intersection_points: Vec<_> = line_calcs.iter_mut().collect();

				intersection_points.sort_by(|a, b| {
					let a_p = a.get_current_intersection_point();
					let b_p = b.get_current_intersection_point();
					if a_p.is_some() && b_p.is_some() {
						a_p.unwrap().cmp_ref_distance(&b_p.unwrap(), &p1)
					} else if a_p.is_none() && b_p.is_some() {
						Ordering::Greater
					} else if a_p.is_some() && b_p.is_none() {
						Ordering::Less
					} else {
						Ordering::Equal
					}
				});

				let line_calc = intersection_points.remove(0);

				match line_calc.get_current_intersection_point() {
					Some(p) => {
						if laser_enabled {
							if let Some(l) = self.create_tool_line(Line2D::new(last_point, p)) {
								tool_lines.push(l);
							}
						}

						last_point = p;
						assert_eq!(line_calc.advance_intersection(), true, "Advancing intersection is expected to be successful");
					}
					None => {
						// No intersections left
						if laser_enabled {
							if let Some(l) = self.create_tool_line(Line2D::new(last_point, p2)) {
								tool_lines.push(l);
							}
						}
						break;
					}
				}
			}
			paths.push(tool_lines)
		}

		paths
	}

	fn create_tool_line(&self, l: Line2D) -> Option<ToolLine> {
		let distance = distance(&l.p1, &l.p2);
		if distance < self.offset * 2.0 {
			None
		} else {
			let line_dir = l.get_direction();
			let shrink_translation = Translation2::from(line_dir.into_inner() * self.offset);
			let shrunk_p1 = shrink_translation * l.p1;
			let shrunk_p2 = shrink_translation.inverse() * l.p2;
			Some(ToolLine::new(Line2D::new(shrunk_p1, shrunk_p2), self.offset))
		}
	}
}

fn project_line_on_rect(dimensions: Vector2<FT>, angle: FT) -> FT {
	// Get angle in range [-PI, PI[
	let angle = ((angle + PI * 0.5) % PI) - PI * 0.5;
	let a = na::distance(&Point::from(Vector2::zeros()), &Point::from(dimensions * 0.5));
	let eps = (dimensions.y / dimensions.x).atan();
	let beta = angle.abs() - eps.abs();
	let b = 2.0 * beta.cos() * a;
	b
}
