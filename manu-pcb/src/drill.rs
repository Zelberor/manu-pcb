// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use io::Error as IoError;
use nalgebra::Transform2;
use std::{
	collections::HashMap,
	io::{self, BufRead},
	num::{ParseFloatError, ParseIntError},
};

use regex::Regex;

use crate::{
	geometry::{Point, FT},
	print::print_warning,
};

#[derive(Debug)]
pub enum Error {
	IoError(IoError),
	ParseError(FileParseError),
}

#[derive(Debug)]
pub enum FileParseError {
	ParseIntError(ParseIntError),
	ParseFloatError(ParseFloatError),
}

impl From<IoError> for Error {
	fn from(value: IoError) -> Self {
		Error::IoError(value)
	}
}

impl From<ParseIntError> for Error {
	fn from(value: ParseIntError) -> Self {
		Error::ParseError(FileParseError::ParseIntError(value))
	}
}

impl From<ParseFloatError> for Error {
	fn from(value: ParseFloatError) -> Self {
		Error::ParseError(FileParseError::ParseFloatError(value))
	}
}

#[derive(Debug, Clone)]
pub struct Hole {
	t: HoleType,
	position: Point,
}

impl Hole {
	fn new(t: HoleType, position: Point) -> Self {
		Hole { t, position }
	}

	pub fn get_type(&self) -> &HoleType {
		&self.t
	}

	pub fn get_position(&self) -> Point {
		self.position
	}

	pub fn transform(&mut self, transform: Transform2<FT>) {
		self.position = transform * self.position;
	}

	pub fn mirror_x(&mut self) {
		self.position.x = -self.position.x
	}

	pub fn mirror_y(&mut self) {
		self.position.y = -self.position.y
	}
}

#[derive(Debug, Clone)]
pub struct HoleType {
	plated: bool,
	via: bool,
	diameter: FT,
}

impl HoleType {
	fn new(plated: bool, via: bool, diameter: FT) -> Self {
		HoleType { plated, via, diameter }
	}

	pub fn is_plated(&self) -> bool {
		self.plated
	}

	pub fn is_via(&self) -> bool {
		self.via
	}

	pub fn diameter(&self) -> FT {
		self.diameter
	}
}

pub fn load_drill_file<R: BufRead>(reader: R) -> Result<Vec<Hole>, Error> {
	let comment_regex = Regex::new(r"^;[^\S\r\n]*#@![^\S\r\n]*TA\.AperFunction,(?<plated>[^\r\n,]+),(?<pth>[^\r\n,]+),(?<drill>[^\r\n,]+)$").unwrap();
	let tool_define_regex = Regex::new(r"^[^\S\r\n]*T(?<index>\d+)C(?<diameter>(\d*\.\d+)|(\d+))[^\S\r\n]*$").unwrap();
	let tool_select_regex = Regex::new(r"^[^\S\r\n]*T(?<index>\d+)[^\S\r\n]*$").unwrap();
	let position_regex = Regex::new(r"^[^\S\r\n]*X(?<x>-?(\d*\.\d+)|(\d+))[^\S\r\n]*Y(?<y>-?(\d*\.\d+)|(\d+))[^\S\r\n]*$").unwrap();

	let mut holes = Vec::new();

	let mut plated = true;
	let mut via = false;

	let mut current_tool: Option<usize> = None;
	let mut tools = HashMap::new();

	for (idx, line) in reader.lines().enumerate() {
		let line = line?;
		if let Some(caps) = comment_regex.captures(&line) {
			let pth = caps["pth"].to_lowercase();
			if pth == "pth" {
				plated = true;
			} else if pth == "npth" {
				plated = false;
			} else {
				print_warning(&format!("Unsupported pth string in line {}: {}", idx, pth));
			}

			let drill = caps["drill"].to_lowercase();
			if drill == "viadrill" {
				via = true;
			} else {
				via = false;
			}
		}

		if let Some(caps) = tool_define_regex.captures(&line) {
			let tool_idx: usize = caps["index"].parse()?;
			let tool_diameter: FT = caps["diameter"].parse()?;

			tools.insert(tool_idx, HoleType::new(plated, via, tool_diameter));
		}

		if let Some(caps) = tool_select_regex.captures(&line) {
			current_tool = Some(caps["index"].parse()?);
		}

		if let Some(caps) = position_regex.captures(&line) {
			let x: FT = caps["x"].parse()?;
			let y: FT = caps["y"].parse()?;

			if let Some(current_tool) = current_tool {
				if let Some(tool) = tools.get(&current_tool) {
					holes.push(Hole::new(tool.clone(), Point::new(x, y)));
				} else {
					print_warning(&format!("For a position in line {}: Tool {} has been selected, but is was not defined.", idx, current_tool))
				}
			} else {
				print_warning(&format!("A hole position has been defined in line {}, but no tool has been selected.", idx))
			}
		}
	}
	Ok(holes)
}
