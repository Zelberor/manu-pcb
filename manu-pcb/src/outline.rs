// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{
	cmp::Ordering,
	f64::{INFINITY, NEG_INFINITY},
	ops::Range,
};

use approx::abs_diff_eq;
use nalgebra::{distance, Transform2};

use crate::geometry::{DistanceTo, IntersectsWith, KicadRoundedEqCmp, Line2D, LineLineIntersection, Point, FT};

#[derive(Debug)]
pub enum OutlineError {
	NotClosed,
}

// A closed path acting as an outline
pub struct Outline {
	points: Vec<Point>,
}

const KICAD_POINT_ON_LINE: FT = 0.001;

impl Outline {
	pub fn from_lines<'a, I: Iterator<Item = &'a Line2D>>(lines: I) -> Result<Vec<Self>, OutlineError> {
		// Vector of (line, already_used); already_used means the line was used for an outline
		let mut lines: Vec<_> = lines
			.filter(|a| {
				// Removes any lines with the same start and end points
				!matches!(a.p1.kicad_roughly_eq_cmp(&a.p2), Ordering::Equal)
			})
			.map(|a| (a, false))
			.collect();
		let mut lines_by_p1: Vec<usize> = (0..lines.len()).collect();
		lines_by_p1.sort_by(|a, b| lines[*a].0.p1.kicad_roughly_eq_cmp(&lines[*b].0.p1));
		let mut lines_by_p2 = lines_by_p1.clone();
		lines_by_p2.sort_by(|a, b| lines[*a].0.p2.kicad_roughly_eq_cmp(&lines[*b].0.p2));

		let mut outlines = Vec::new();
		for line_idx in &lines_by_p1 {
			if lines[*line_idx].1 {
				// Line has already been processed
				continue;
			}
			lines[*line_idx].1 = true;
			let line = lines[*line_idx].0;
			let mut outline = Outline { points: Vec::new() };
			outline.points.push(line.p1);
			outline.points.push(line.p2);
			loop {
				let len = outline.points.len();
				let search_point = outline.points.last().expect("Expected first two points to be added");
				let mut next_point = None;
				{
					// Search for search_point by comparing with all p1
					let l_by_p1_indices = binary_search_all_by(&lines_by_p1, |a| lines[*a].0.p1.kicad_roughly_eq_cmp(search_point));
					for idx in l_by_p1_indices {
						let line_idx = lines_by_p1[idx];
						if !lines[line_idx].1 {
							// Line was not yet processed; it becomes the next point
							next_point = Some(lines[line_idx].0.p2);
							lines[line_idx].1 = true;
							break;
						}
					}
				}
				if next_point.is_none() {
					// Try search again now comparing with all p2
					let l_by_p2_indices = binary_search_all_by(&lines_by_p2, |a| lines[*a].0.p2.kicad_roughly_eq_cmp(search_point));
					for idx in l_by_p2_indices {
						let line_idx = lines_by_p2[idx];
						if !lines[line_idx].1 {
							// Line was not yet processed; it becomes the next point
							next_point = Some(lines[line_idx].0.p1);
							lines[line_idx].1 = true;
							break;
						}
					}
				}

				if let Some(p) = next_point {
					// Merge points that are in a line
					let p_m_1 = outline.points[len - 1];
					let p_m_2 = outline.points[len - 2];
					if Line2D::new(p_m_2, p_m_1).distance_to(&p).abs() <= KICAD_POINT_ON_LINE {
						outline.points.pop();
					}

					if matches!(outline.points.first().unwrap().kicad_roughly_eq_cmp(&p), Ordering::Equal) {
						// Outline is closed
						break;
					}
					outline.points.push(p);
				} else {
					return Err(OutlineError::NotClosed);
				}
			}

			if outline.points.len() < 3 {
				return Err(OutlineError::NotClosed);
			}

			outlines.push(outline);
		}

		Ok(outlines)
	}

	pub fn get_bounding_rect(&self) -> (Point, Point) {
		let mut min_x = INFINITY;
		let mut max_x = NEG_INFINITY;
		let mut min_y = INFINITY;
		let mut max_y = NEG_INFINITY;

		for p in &self.points {
			if p.x < min_x {
				min_x = p.x;
			}
			if p.x > max_x {
				max_x = p.x;
			}
			if p.y < min_y {
				min_y = p.y;
			}
			if p.y > max_y {
				max_y = p.y;
			}
		}
		(Point::new(min_x, min_y), Point::new(max_x, max_y))
	}

	pub fn get_segment(&self, index: usize) -> Line2D {
		Line2D::new(self.points[index], self.points[(index + 1) % self.points.len()])
	}

	pub fn get_segment_iter(&self) -> impl Iterator<Item = Line2D> + '_ {
		SegmentIterator::new(self)
	}

	pub fn get_segment_num(&self) -> usize {
		self.points.len()
	}

	pub fn transform(&mut self, transform: Transform2<FT>) {
		for p in &mut self.points {
			*p = transform * *p;
		}
	}
}

struct SegmentIterator<'a> {
	outline: &'a Outline,
	index: usize,
}

impl<'a> SegmentIterator<'a> {
	fn new(outline: &'a Outline) -> Self {
		SegmentIterator { outline, index: 0 }
	}
}

impl<'a> Iterator for SegmentIterator<'a> {
	type Item = Line2D;

	fn next(&mut self) -> Option<Self::Item> {
		if self.index < self.outline.points.len() {
			let line = self.outline.get_segment(self.index);
			self.index += 1;
			Some(line)
		} else {
			None
		}
	}
}

pub enum OutlineLineIntersectionType {
	Point(Point),
	Coincident(Point, Point),
}

pub struct OutlineLineIntersection {
	pub t: OutlineLineIntersectionType,
	pub segment_index: usize,
	/// Intersection oversteps the outline
	pub overstep: bool,
}

impl OutlineLineIntersection {
	pub fn get_near_point(&self, p: &Point) -> Point {
		match self.t {
			OutlineLineIntersectionType::Point(ip) => ip,
			OutlineLineIntersectionType::Coincident(ip1, ip2) => {
				let dist_1 = distance(&p, &ip1);
				let dist_2 = distance(&p, &ip2);
				if dist_1 < dist_2 {
					ip1
				} else {
					ip2
				}
			}
		}
	}

	pub fn get_far_point(&self, p: &Point) -> Point {
		match self.t {
			OutlineLineIntersectionType::Point(ip) => ip,
			OutlineLineIntersectionType::Coincident(ip1, ip2) => {
				let dist_1 = distance(&p, &ip1);
				let dist_2 = distance(&p, &ip2);
				if dist_1 < dist_2 {
					ip2
				} else {
					ip1
				}
			}
		}
	}
}

impl IntersectsWith<Line2D> for Outline {
	type Intersections = Vec<OutlineLineIntersection>;

	fn intersects_with(&self, t: &Line2D) -> Self::Intersections {
		let mut last_intersection: Option<(&OutlineLineIntersection, Line2D)> = None;
		let mut intersections: Vec<OutlineLineIntersection> = Vec::new();

		// Includes the first line at the start and the end to properly process last_intersections
		let mut iter_idx = 0;
		loop {
			let idx = iter_idx % self.get_segment_num();
			let line = self.get_segment(idx);

			let (new_intersection, pop_last) = match line.intersects_with(t) {
				LineLineIntersection::Point { p, inside_bounds } => {
					if inside_bounds {
						let mut intersection_already_present = false;
						let mut overstep = true;
						if let Some((i, last_line)) = last_intersection {
							match i.t {
								OutlineLineIntersectionType::Point(last_p) => {
									// When two intersections have the same point, the according outline segments intersected at their ends
									if abs_diff_eq!(p, last_p) {
										intersection_already_present = true;
										let line_other_point = line.p2;
										let last_line_other_point = last_line.p1;
										// When both the last outline_line and the current outline_line are on the same side of t, the intersection does not overstep
										if t.distance_to(&line_other_point).signum() == t.distance_to(&last_line_other_point).signum() {
											overstep = false;
										}
									}
								}
								OutlineLineIntersectionType::Coincident(_, _) => {
									if abs_diff_eq!(t.distance_to(&p), 0.0) {
										intersection_already_present = true;
									}
								}
							}
						}

						if !intersection_already_present {
							(
								Some(OutlineLineIntersection {
									t: OutlineLineIntersectionType::Point(p),
									segment_index: idx,
									overstep,
								}),
								false,
							)
						} else {
							(None, false)
						}
					} else {
						(None, false)
					}
				}
				LineLineIntersection::Coincident { p1, p2, overlaps } => {
					if overlaps {
						let mut pop_last = false;

						if let Some((i, _)) = last_intersection {
							match i.t {
								OutlineLineIntersectionType::Point(last_p) => {
									// last intersection point is on coincident line
									if abs_diff_eq!(t.distance_to(&last_p), 0.0) {
										pop_last = true;
									}
								}
								OutlineLineIntersectionType::Coincident(_, _) => {
									panic!("Consecutive coincident intersections are unsupported by outlines. Coincident sections must be merged.")
								}
							}
						}
						let last_line = self.get_segment((idx as i64 - 1).rem_euclid(self.get_segment_num() as i64) as usize);
						let next_line = self.get_segment((idx + 1) % self.get_segment_num());
						let overstep = if t.distance_to(&last_line.p1).signum() == t.distance_to(&next_line.p2).signum() { false } else { true };

						(
							Some(OutlineLineIntersection {
								t: OutlineLineIntersectionType::Coincident(p1, p2),
								segment_index: idx,
								overstep,
							}),
							pop_last,
						)
					} else {
						(None, false)
					}
				}
				LineLineIntersection::Parallel => (None, false),
			};

			if pop_last {
				intersections.pop();
			}

			// Check if we are done
			if iter_idx == self.get_segment_num() {
				// Replace first intersection, if it exists
				if let Some(i) = intersections.get(0) {
					if i.segment_index == 0 {
						if let Some(new_i) = new_intersection {
							intersections[0] = new_i;
						} else {
							intersections.remove(0);
						}
					}
				}
				break;
			}

			last_intersection = if let Some(i) = new_intersection {
				intersections.push(i);
				Some((intersections.last().unwrap(), line.clone()))
			} else {
				None
			};

			iter_idx += 1;
		}

		intersections
	}
}

fn binary_search_all_by<'a, T, F>(s: &'a [T], f: F) -> Range<usize>
where
	F: Fn(&T) -> Ordering,
{
	let low = s.partition_point(|a| {
		let ord = f(a);
		matches!(ord, Ordering::Less)
	});
	let high = s.partition_point(|a| {
		let ord = f(a);
		matches!(ord, Ordering::Less) || matches!(ord, Ordering::Equal)
	});
	return low..high;
}
