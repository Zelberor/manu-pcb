// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod snapmaker;

use std::io::Error as IoError;
use std::io::Write;

pub use super::{GcodeCommand, GcodeStats, MoveType, ToolType};

#[derive(Debug)]
pub enum GcodeWriteError {
	IoError(IoError),
	FlavourError(FlavourError),
}

impl From<FlavourError> for GcodeWriteError {
	fn from(value: FlavourError) -> Self {
		GcodeWriteError::FlavourError(value)
	}
}

impl From<IoError> for GcodeWriteError {
	fn from(value: IoError) -> Self {
		GcodeWriteError::IoError(value)
	}
}

#[derive(Debug)]
pub enum FlavourError {
	UnsupportedCommand(GcodeCommand),
	IgnoredCommand(GcodeCommand),
}

pub trait FlavourWrite {
	fn write_gcode<W: Write>(&self, writer: &mut W, commands: &[GcodeCommand], stats: &GcodeStats) -> Result<(), GcodeWriteError>;

	fn command_to_str(&self, c: &GcodeCommand) -> Result<String, FlavourError>;
}

pub struct Generic {}

impl FlavourWrite for Generic {
	fn write_gcode<W: Write>(&self, mut writer: &mut W, commands: &[GcodeCommand], _stats: &GcodeStats) -> Result<(), GcodeWriteError> {
		let gcode_header = "G21 ; set units to millimeters
G90 ; absolute positioning
";

		write!(&mut writer, "{}", gcode_header)?;

		write_commands(self, commands.iter(), &mut writer)?;

		Ok(())
	}

	fn command_to_str(&self, c: &GcodeCommand) -> Result<String, FlavourError> {
		match c {
			GcodeCommand::ToolOn { power } => match power {
				crate::gcode::ToolPower::PowerPercentage(p) => Ok(format!("M3 P{} S{}", (p * 100.0) as u32, ((*p * 255.0) as u32))),
				crate::gcode::ToolPower::SpindleRpm(_) => Err(FlavourError::UnsupportedCommand(c.clone())),
			},
			GcodeCommand::ToolOff => Ok("M5".to_owned()),
			GcodeCommand::Move { t, x, y, z, feedrate } => Ok({
				let command = match t {
					MoveType::Rapid => "G0".to_owned(),
					MoveType::Tool => "G1".to_owned(),
				};
				let x_code = if let Some(x) = x { format!("X{}", x) } else { "".to_owned() };
				let y_code = if let Some(y) = y { format!("Y{}", y) } else { "".to_owned() };
				let z_code = if let Some(z) = z { format!("Z{}", z) } else { "".to_owned() };
				format!("{} {} {} {} F{}", command, x_code, y_code, z_code, feedrate)
			}),
			GcodeCommand::Dwell { time_ms } => Ok(format!("G4 P{}", time_ms)),
			GcodeCommand::MoveArc { ccw, center_offset, end_pos, feedrate } => {
				let code = match ccw {
					true => "G3",
					false => "G2",
				};
				let pos_code = if let Some(p) = end_pos { format!("X{} Y{}", p.x, p.y) } else { "".to_owned() };
				Ok(format!("{} {} I{} J{} F{}", code, pos_code, center_offset.x, center_offset.y, feedrate))
			}
		}
	}
}

pub fn write_commands<'a, F: FlavourWrite, W: Write, I: Iterator<Item = &'a GcodeCommand>>(flavour: &F, commands: I, writer: &mut W) -> Result<(), GcodeWriteError> {
	for command in commands {
		write!(writer, "{}\n", flavour.command_to_str(command)?)?
	}
	Ok(())
}
