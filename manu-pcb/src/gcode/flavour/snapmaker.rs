// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::io::Write;

use super::{FlavourError, FlavourWrite, GcodeCommand, GcodeStats, GcodeWriteError, MoveType, ToolType};

pub struct Snapmaker {
	tool_type: ToolType,
}

impl Snapmaker {
	pub fn new(tool_type: ToolType) -> Self {
		Snapmaker { tool_type }
	}
}

impl FlavourWrite for Snapmaker {
	fn write_gcode<W: Write>(&self, mut writer: &mut W, commands: &[GcodeCommand], stats: &GcodeStats) -> Result<(), GcodeWriteError> {
		let line_num = 35 + stats.line_count;

		let (header_type, tool_head) = match self.tool_type {
			ToolType::Laser => ("laser", "levelTwoLaserToolheadForSM2"),
			ToolType::Cnc => ("cnc", "standardCNCToolheadForSM2"),
		};

		let gcode_header = format!(
			";Header Start
;header_type: {}
;tool_head: {}
;machine: A350
;renderMethod: line
;file_total_lines: {}
;estimated_time(s): {}
;is_rotate: false
;diameter: 40
;max_x(mm): {}
;max_y(mm): {}
;max_z(mm): 0
;max_b(mm): 0
;min_x(mm): {}
;min_y(mm): {}
;min_b(mm): 0
;min_z(mm): 0
;work_speed(mm/minute): 2000
;jog_speed(mm/minute): 3000
;power(%): 100
;work_size_x: 320
;work_size_y: 350
;origin: center
;thumbnail: data:image/gif;base64,R0lGODlhAQABAIAAAP7//wAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==,
;Header End

G21 ; set units to millimeters
G90 ; absolute positioning

M201 X200 Y200 ; lower accel

M106 P0 S255; Fan on
",
			header_type,
			tool_head,
			line_num,
			stats.duration.as_secs_f64(),
			stats.max_p.x,
			stats.max_p.y,
			stats.min_p.x,
			stats.min_p.y
		);

		write!(&mut writer, "{}", gcode_header)?;

		super::write_commands(self, commands.iter(), &mut writer)?;

		let gcode_footer = "
M107 P0; Fan off
		";

		write!(&mut writer, "{}", gcode_footer)?;

		Ok(())
	}

	fn command_to_str(&self, c: &GcodeCommand) -> Result<String, FlavourError> {
		match c {
			GcodeCommand::ToolOn { power } => {
				let power_percentage = match power {
					crate::gcode::ToolPower::PowerPercentage(p) => Ok(*p),
					crate::gcode::ToolPower::SpindleRpm(r) => {
						if matches!(self.tool_type, ToolType::Cnc) {
							Ok(*r as f32 / 12000.0)
						} else {
							Err(FlavourError::UnsupportedCommand(c.clone()))
						}
					}
				}?;
				Ok(format!("M3 P{} S{}", (power_percentage * 100.0) as u32, ((power_percentage * 255.0) as u32)))
			}
			GcodeCommand::ToolOff => Ok("M5".to_owned()),
			GcodeCommand::Move { t, x, y, z, feedrate } => Ok({
				let command = match t {
					MoveType::Rapid => "G0".to_owned(),
					MoveType::Tool => "G1".to_owned(),
				};
				let x_code = if let Some(x) = x { format!("X{}", x) } else { "".to_owned() };
				let y_code = if let Some(y) = y { format!("Y{}", y) } else { "".to_owned() };
				let z_code = if let Some(z) = z { format!("Z{}", z) } else { "".to_owned() };
				format!("{} {} {} {} F{}", command, x_code, y_code, z_code, feedrate)
			}),
			GcodeCommand::Dwell { time_ms } => Ok(format!("G4 P{}", time_ms)),
			GcodeCommand::MoveArc { ccw, center_offset, end_pos, feedrate } => {
				let code = match ccw {
					true => "G3",
					false => "G2",
				};
				let pos_code = if let Some(p) = end_pos { format!("X{} Y{}", p.x, p.y) } else { "".to_owned() };
				Ok(format!("{} {} I{} J{} F{}", code, pos_code, center_offset.x, center_offset.y, feedrate))
			}
		}
	}
}
