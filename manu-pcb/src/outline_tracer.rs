// Copyright 2023 Peter Viechter
//
// This file is part of manu-pcb.
//
// manu-pcb is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// manu-pcb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{f64::consts::PI, iter::zip, ops::Range};

use nalgebra::{distance, Rotation2, Translation2};

use crate::{
	geometry::{IntersectsWith, Line2D, LineLineIntersection, Point, FT},
	line_tracer::LineTracer,
	outline::Outline,
	toolpath::ToolPath,
};

#[derive(Debug)]
pub enum OutlineTracerError {
	NoSpace,
	NoStartPoint,
}

pub struct OutlineTracer {
	/// Offset to all outlines. "Radius" of the tool
	offset: FT,
}

impl OutlineTracer {
	pub fn new(offset: FT) -> Self {
		OutlineTracer { offset }
	}

	pub fn create_tool_path(&self, outline: &Outline, edges: &[Outline], invert: bool) -> Result<ToolPath, OutlineTracerError> {
		let mut is_pos_offset = None;
		for line in outline.get_segment_iter() {
			let dir = line.get_direction();
			let dir_90 = Rotation2::new(PI / 2.0) * dir;
			let middle = line.get_middle();

			let pos_point = Translation2::from(dir_90.into_inner() * self.offset) * middle;
			let neg_point = Translation2::from(-dir_90.into_inner() * self.offset) * middle;
			let mut mill_active = [false, false];
			for (p, mill_active) in zip([pos_point, neg_point], &mut mill_active) {
				let tracer = LineTracer::new(Line2D::new(Point::new(0.0, 0.0), p), 0.0, edges.iter(), !invert);
				*mill_active = tracer.get_final_enabled().1;
			}

			if mill_active[0] && !mill_active[1] {
				is_pos_offset = Some(true)
			} else if mill_active[1] && !mill_active[0] {
				is_pos_offset = Some(false)
			}

			if is_pos_offset.is_some() {
				break;
			}
		}

		match is_pos_offset {
			Some(is_pos_offset) => {
				let mut points = trace_outline(outline, if is_pos_offset { self.offset } else { -self.offset });

				if points.len() <= 1 {
					Err(OutlineTracerError::NoSpace)
				} else {
					let first = *points.first().unwrap();
					points.push(first);
					Ok(ToolPath::new(&points, self.offset))
				}
			}
			None => Err(OutlineTracerError::NoStartPoint),
		}
	}
}

fn trace_outline(outline: &Outline, distance: FT) -> Vec<Point> {
	let mut tool_points = Vec::new();

	// Build a path offset by distance
	let mut last_segment = outline.get_segment(outline.get_segment_num() - 1);
	for segment in outline.get_segment_iter() {
		let last_line = last_segment.parallel(distance);
		let line = segment.parallel(distance);

		let next_point = match line.intersects_with(&last_line) {
			LineLineIntersection::Point { p, inside_bounds: _ } => p,
			LineLineIntersection::Coincident { p1: _, p2: _, overlaps: _ } | LineLineIntersection::Parallel => panic!("Outline expected to not have consecutive coincident lines."),
		};
		tool_points.push(next_point);

		last_segment = segment;
	}

	// Check for intersections between any segments and remove them
	let mut base_idx = 0;
	loop {
		let tool_points_num = tool_points.len();
		if base_idx >= tool_points_num - 2 {
			break;
		}
		let mut skip_base_inc = false;

		let base_line = Line2D::new(tool_points[base_idx], tool_points[base_idx + 1]);
		let end_idx = if base_idx == 0 { tool_points_num - 1 } else { tool_points_num };
		for idx in base_idx + 2..end_idx {
			let line = Line2D::new(tool_points[idx], tool_points[(idx + 1) % tool_points_num]);

			if let LineLineIntersection::Point { p, inside_bounds: true } = base_line.intersects_with(&line) {
				// Distance of the both lines in points in both directions around the toolpath
				let total_distance = points_calculate_distance(&tool_points, 0..tool_points_num);
				let p_distance = points_calculate_distance(&tool_points, base_idx + 1..idx) + nalgebra::distance(&p, &base_line.p2) + nalgebra::distance(&line.p1, &p);
				let p_inv_distance = total_distance - p_distance;

				if p_distance <= p_inv_distance {
					for mod_p in &mut tool_points[base_idx + 1..=idx] {
						*mod_p = p;
					}
				} else {
					for mod_p in &mut tool_points[0..=base_idx] {
						*mod_p = p;
					}
					for mod_p in &mut tool_points[idx + 1..tool_points_num] {
						*mod_p = p;
					}
				}

				tool_points = merge_duplicate_points(&tool_points);
				skip_base_inc = true;
				base_idx = 0;
				break;
			}
		}

		if !skip_base_inc {
			base_idx += 1;
		}
	}

	tool_points
}

fn merge_duplicate_points(points: &[Point]) -> Vec<Point> {
	let mut last_point = *points.last().unwrap();
	points
		.iter()
		.filter_map(|p| {
			if *p == last_point {
				None
			} else {
				last_point = *p;
				Some(*p)
			}
		})
		.collect()
}

fn points_calculate_distance(points: &[Point], range: Range<usize>) -> FT {
	let mut d = 0.0;
	for idx in range {
		let p1 = &points[idx];
		let p2 = &points[(idx + 1) % points.len()];
		d += distance(p1, p2);
	}
	d
}
