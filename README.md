# Manu-Pcb
Converts dxf files exported from KiCad into gcode for laser engraving

## Installation
- Clone the repository
- In terminal, change into repository root folder
- Execute `cargo install --path ./manu-pcb`
- Make sure `~/.cargo/bin` is in your *PATH*
- Optional: Install the [kicad plugin](./kicad-plugin/README.md)

## Usage
Execute `manu-pcb help`
